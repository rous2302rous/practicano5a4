package ito.poo.app;
import ito.poo.clases.Ubicacion;
import ito.poo.clases.CuerpoCeleste;

public class MyApp {

	public static void run() {
		CuerpoCeleste c = new CuerpoCeleste("Archi", "hierro");
		
		c.agregar(new Ubicacion(150f, 130f, "Enero"));
		
		System.out.println("La distancia que se desplaso es: "+c.desplasamiento(250f, 230f));
		
	}
	
	public static void main(String[] args) {
		
		run();
		
	}

}
