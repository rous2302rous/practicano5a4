package ito.poo.clases;

public class Ubicacion {

	
	private float longitud;
	private float latitud;
	private float Distancia;
	private String periodo;
	
	public Ubicacion(float longitud, float latitud, String periodo) {
		super();
		this.longitud = longitud;
		this.latitud = latitud;
		this.periodo = periodo;
	}
	
	public float getLongitud() {
		return longitud;
	}

	public void setLongitud(float longitud) {
		this.longitud = longitud;
	}

	public float getLatitud() {
		return latitud;
	}

	public void setLatitud(float latitud) {
		this.latitud = latitud;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	
	public float getDistancia() {
		return Distancia;
	}



	public void setDistancia(float distancia) {
		Distancia = distancia;
	}

	@Override
	public String toString() {
		return "Ubicacion [longitud=" + longitud + ", latitud=" + latitud + ", distancia="+ Distancia + ", periodo=" + periodo + "]";
	}
	
	
	
}
