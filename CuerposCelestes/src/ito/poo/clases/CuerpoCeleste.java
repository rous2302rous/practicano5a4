package ito.poo.clases;
import ito.poo.clases.Ubicacion;

import java.util.ArrayList;

public class CuerpoCeleste {

	private String nombre;
	private String Composicion;
	private ArrayList<Ubicacion> Ubicacion;
	
	public CuerpoCeleste(String nombre, String composicion) {
		super();
		this.nombre = nombre;
		this.Composicion = composicion;
		this.Ubicacion = new ArrayList<Ubicacion>();
		
	}
	
	public void agregar(Ubicacion p) {
		
		Ubicacion.add(p);
		
	}
	
	public float desplasamiento(float i, float j) {
		Ubicacion u = new Ubicacion(150f, 130f, "Abril");
		
		float n = i-u.getLatitud();
		float b = j-u.getLatitud();
		
		float r = n+b;
		
		return r;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getComposicion() {
		return Composicion;
	}

	public void setComposicion(String composicion) {
		Composicion = composicion;
	}

	@Override
	public String toString() {
		return "CuerpoCeleste [nombre=" + nombre + ", Composicion=" + Composicion + ", Ubicacion=" + Ubicacion + "]";
	}
	
	
	
}
